#ifndef PERFORMANCE_COUNTERS_HPP
#define PERFORMANCE_COUNTERS_HPP

// Import Godot support classes
#include <godot_cpp/core/class_db.hpp>

// Import base class
#include <godot_cpp/classes/object.hpp>

// Include platform specific things
#ifdef _WIN32
// Windows only
#include <windows.h>
#include <psapi.h>
#include <pdh.h>
#include <nvml.h>
#endif

namespace godot {
	class PerformanceCounters : public Object {
		GDCLASS(PerformanceCounters, Object)

		private:

			// Windows only
#ifdef _WIN32
			PDH_HQUERY _win_cpu_query;
			PDH_HCOUNTER _win_cpu_counter;

			int win_init_cpu_counter();
			float win_query_cpu_counter();
			int win_deinit_cpu_counter();

			int win_init_process_cpu_counter();
			float win_query_process_cpu_counter();
			int win_deinit_process_cpu_counter();

			nvmlDevice_t _win_gpu_nv_device;

			int win_init_gpu_counter();
			float win_query_gpu_counter();
			int win_deinit_gpu_counter();

			int win_init_process_gpu_counter();
			float win_query_process_gpu_counter();
			int win_deinit_process_gpu_counter();

			int win_init_gpu_temperature_counter();
			float win_query_gpu_temperature_counter();
			int win_deinit_gpu_temperature_counter();

			int win_init_gpu_power_counter();
			float win_query_gpu_power_counter();
			int win_deinit_gpu_power_counter();
#endif

		public:
			PerformanceCounters();
			~PerformanceCounters();

			// CPU queries
			float query_cpu_usage();
			float query_process_cpu_usage();

			// GPU queries
			float query_gpu_temperature();
			float query_gpu_usage();
			float query_process_gpu_usage();
			float query_gpu_power();

		protected:
			static void _bind_methods();
	};

}

#endif
