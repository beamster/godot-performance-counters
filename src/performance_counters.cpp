#include "performance_counters.hpp"

#include <godot_cpp/variant/utility_functions.hpp>

using namespace godot;

/**
 * Establish methods we expose to GDScript and the Godot UI.
 */
void PerformanceCounters::_bind_methods() {
	ClassDB::bind_method(D_METHOD("query_process_cpu_usage"), &PerformanceCounters::query_process_cpu_usage);
	ClassDB::bind_method(D_METHOD("query_cpu_usage"), &PerformanceCounters::query_cpu_usage);
	ClassDB::bind_method(D_METHOD("query_process_gpu_usage"), &PerformanceCounters::query_process_gpu_usage);
	ClassDB::bind_method(D_METHOD("query_gpu_usage"), &PerformanceCounters::query_gpu_usage);
	ClassDB::bind_method(D_METHOD("query_gpu_temperature"), &PerformanceCounters::query_gpu_temperature);
	ClassDB::bind_method(D_METHOD("query_gpu_power"), &PerformanceCounters::query_gpu_power);
}

/**
 * Constructor.
 */
PerformanceCounters::PerformanceCounters() {
#ifdef _WIN32
	this->win_init_process_cpu_counter();
	this->win_init_cpu_counter();
	this->win_init_process_gpu_counter();
	this->win_init_gpu_counter();
	this->win_init_gpu_temperature_counter();
	this->win_init_gpu_power_counter();
#endif
}

/**
 * Clean up.
 */
PerformanceCounters::~PerformanceCounters() {
#ifdef _WIN32
	this->win_deinit_process_cpu_counter();
	this->win_deinit_cpu_counter();
	this->win_deinit_process_gpu_counter();
	this->win_deinit_gpu_counter();
	this->win_deinit_gpu_temperature_counter();
	this->win_deinit_gpu_power_counter();
#endif
}

/**
 * Query system cpu counter.
 */
float PerformanceCounters::query_cpu_usage() {
#ifdef _WIN32
	return this->win_query_cpu_counter();
#else
	return -1;
#endif
}

/**
 * Query cpu counter.
 */
float PerformanceCounters::query_process_cpu_usage() {
#ifdef _WIN32
	return this->win_query_process_cpu_counter();
#else
	return -1;
#endif
}

/**
 * Query system gpu counter.
 */
float PerformanceCounters::query_gpu_usage() {
#ifdef _WIN32
	return this->win_query_gpu_counter();
#else
	return -1;
#endif
}

/**
 * Query system gpu temperature.
 */
float PerformanceCounters::query_gpu_temperature() {
#ifdef _WIN32
	return this->win_query_gpu_temperature_counter();
#else
	return -1;
#endif
}

/**
 * Query system gpu power in milliwatts.
 */
float PerformanceCounters::query_gpu_power() {
#ifdef _WIN32
	return this->win_query_gpu_power_counter();
#else
	return -1;
#endif
}

/**
 * Query gpu counter.
 */
float PerformanceCounters::query_process_gpu_usage() {
#ifdef _WIN32
	return this->win_query_process_gpu_counter();
#else
	return -1;
#endif
}

/**
 * Windows implementation to initialize the cpu counter.
 */
#ifdef _WIN32
int PerformanceCounters::win_init_cpu_counter() {
	return 0;
}

float PerformanceCounters::win_query_cpu_counter() {
	return -1.0;
}

int PerformanceCounters::win_deinit_cpu_counter() {
	return 0;
}

// Turn a process file path to a process name.
static std::wstring _ImageFileNameToProcessName(std::wstring buffer) {
	std::wstring fullPath = buffer;
	size_t ext_pos = fullPath.rfind(L".exe");
	if (ext_pos != std::wstring::npos) {
		// Truncate file extension
		fullPath = fullPath.substr(0, ext_pos);
	}

	size_t pos = fullPath.rfind('\\');
	if (pos != std::wstring::npos) {
		return fullPath.substr(pos + 1);  // return only the process name
	}
	return L"";
}

// Return the current process file path.
static std::wstring _GetCurrentProcessName() {
	WCHAR buffer[MAX_PATH];
	if (GetProcessImageFileNameW(GetCurrentProcess(), buffer, MAX_PATH)) {
		return _ImageFileNameToProcessName(buffer);
	}
	return L"";
}

// Given a process ID, find the associated PDH instance name
// (Another way to do this is to just query the name with * wildcard and
//  also track the "ID Process" counter)
static std::wstring _GetProcessInstanceName(DWORD pid) {
	DWORD aProcesses[1024], cbNeeded;
	if (!EnumProcesses(aProcesses, sizeof(aProcesses), &cbNeeded)) {
		return L"";
	}

	// What is the process name we are comparing against.
	std::wstring currentInstance = _GetCurrentProcessName();

	int found = 0;
	DWORD cProcesses = cbNeeded / sizeof(DWORD);
	for (unsigned int i = 0; i < cProcesses; i++) {
		if (aProcesses[i] != 0) {
			HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, aProcesses[i]);
			if (hProcess) {
				WCHAR buffer[MAX_PATH];
				if (GetProcessImageFileNameW(hProcess, buffer, MAX_PATH)) {
					std::wstring instanceName = _ImageFileNameToProcessName(buffer);
					if (pid == aProcesses[i]) {
						CloseHandle(hProcess);

						if (found > 0) {
							instanceName += L"#" + std::to_wstring(found);
						}

						return instanceName;
					}
					else if (instanceName.compare(currentInstance) == 0) {
						found++;
					}

				}
				CloseHandle(hProcess);
			}
		}
	}

	return L"";
}

int PerformanceCounters::win_init_process_cpu_counter() {
	// Get a handle to the CPU perf counter.
	if (PdhOpenQueryW(NULL, 0, &this->_win_cpu_query) != ERROR_SUCCESS) {
		return -1;
	}

	// Determine the PDH process name
	DWORD pid = GetCurrentProcessId();
	std::wstring process_name = _GetProcessInstanceName(pid);

	// Get our process name
	std::wstring counter = L"\\Process(" + process_name + L")\\% User Time";

	if (PdhAddEnglishCounterW(this->_win_cpu_query, counter.c_str(), 0, &this->_win_cpu_counter) != ERROR_SUCCESS || PdhCollectQueryData(this->_win_cpu_query) != ERROR_SUCCESS) {
		PdhCloseQuery(this->_win_cpu_query);
		return -2;
	}

	return 0;
}

float PerformanceCounters::win_query_process_cpu_counter() {
	PDH_FMT_COUNTERVALUE counter_val;

	if (PdhCollectQueryData(this->_win_cpu_query) != ERROR_SUCCESS || PdhGetFormattedCounterValue(this->_win_cpu_counter, PDH_FMT_DOUBLE | PDH_FMT_NOCAP100, NULL, &counter_val) != ERROR_SUCCESS) {
		return -1;
	}

	// Get the number of logical cores
	SYSTEM_INFO sysInfo;
	GetSystemInfo(&sysInfo);
	DWORD numberOfLogicalCores = sysInfo.dwNumberOfProcessors;

	return counter_val.doubleValue / (float)(numberOfLogicalCores);
}

int PerformanceCounters::win_deinit_process_cpu_counter() {
	if (this->_win_cpu_query != NULL) {
		PdhCloseQuery(this->_win_cpu_query);
		this->_win_cpu_query = NULL;
	}

	return 0;
}

int PerformanceCounters::win_init_process_gpu_counter() {
	// Unimplemented (per process GPU usage)
	return 0;
}

float PerformanceCounters::win_query_process_gpu_counter() {
	// Unimplemented (per process GPU usage)
	return -1.0;
}

int PerformanceCounters::win_deinit_process_gpu_counter() {
	// Unimplemented (per process GPU usage)
	return 0;
}

int PerformanceCounters::win_init_gpu_counter() {
	unsigned int deviceCount, i;
	nvmlReturn_t result;
	unsigned int pidToSearch = GetCurrentProcessId();

	// Init the device handle
	this->_win_gpu_nv_device = NULL;

	// Initialize NVML
	result = nvmlInit();
	if (result != NVML_SUCCESS) {
		// No NVidia driver or card, perhaps
		return 1;
	}

	// Get the number of devices
	result = nvmlDeviceGetCount(&deviceCount);
	if (result != NVML_SUCCESS) {
		UtilityFunctions::print("Failed to get device count: ", nvmlErrorString(result));
		nvmlShutdown();
		return 1;
	}

	if (deviceCount == 0) {
		UtilityFunctions::print("Failed to see any compatible devices.");
		nvmlShutdown();
		return 1;
	}

	// Iterate over each device to find the one our process is running upon
	for (i = 0; i < deviceCount; i++) {
		nvmlDevice_t device;

		result = nvmlDeviceGetHandleByIndex(i, &device);
		if (result != NVML_SUCCESS) {
			UtilityFunctions::print("Failed to get handle for device ", i , ": ", nvmlErrorString(result));
			continue;
		}

		// Check running processes
		nvmlProcessInfo_t processes[64];
		unsigned int processCount = sizeof(processes) / sizeof(processes[0]);

		result = nvmlDeviceGetComputeRunningProcesses(device, &processCount, processes);
		if (result != NVML_SUCCESS) {
			UtilityFunctions::print("Failed to get compute running processes for device ", i, ": ", nvmlErrorString(result));
			continue;
		}

		for (unsigned int j = 0; j < processCount; j++) {
			if (processes[j].pid == pidToSearch) {
				// Set the device
				this->_win_gpu_nv_device = device;
				break;
			}
		}
	}

	return 0;
}

float PerformanceCounters::win_query_gpu_counter() {
	nvmlReturn_t result;
	nvmlUtilization_t utilization;

	if (!this->_win_gpu_nv_device) {
		return 0.0;
	}

	result = nvmlDeviceGetUtilizationRates(this->_win_gpu_nv_device, &utilization);
	return utilization.gpu;
}

int PerformanceCounters::win_deinit_gpu_counter() {
	// Shutdown NVML
	nvmlShutdown();
	return 0;
}

int PerformanceCounters::win_init_gpu_temperature_counter() {
	// No need to do anything
	return 0;
}

float PerformanceCounters::win_query_gpu_temperature_counter() {
	nvmlReturn_t result;
	unsigned int temp = 0;

	if (!this->_win_gpu_nv_device) {
		return 0.0;
	}

	result = nvmlDeviceGetTemperature(this->_win_gpu_nv_device, NVML_TEMPERATURE_GPU, &temp);
	return (float)temp;
}

int PerformanceCounters::win_deinit_gpu_temperature_counter() {
	// No need to do anything
	return 0;
}

int PerformanceCounters::win_init_gpu_power_counter() {
	// No need to do anything
	return 0;
}

float PerformanceCounters::win_query_gpu_power_counter() {
	nvmlReturn_t result;
	unsigned int power;

	if (!this->_win_gpu_nv_device) {
		return 0.0;
	}

	result = nvmlDeviceGetPowerUsage(this->_win_gpu_nv_device, &power);
	return (float)power;
}

int PerformanceCounters::win_deinit_gpu_power_counter() {
	// No need to do anything
	return 0;
}
#endif
