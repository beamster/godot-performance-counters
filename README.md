# Godot - PerformanceCounters

This gdextension adds the `PerformanceCounters` class which has methods to query
the hardware performance related to the running project.

Right now, this is very humble. It has the following functions:

* `query_cpu_usage` `-> float`: Returns the percentage of the total CPU resources in use by the system as a float between `0.0` and `100.0`.
* `query_process_cpu_usage` `-> float`: Returns the percentage of the total CPU resources in use by the process as a float between `0.0` and `100.0`.
* `query_gpu_usage` `-> float`: Returns the percentage of the total GPU resources in use by the system as a float between `0.0` and `100.0`.
* `query_process_gpu_usage` `-> float`: Returns the percentage of the total GPU resources in use by the process as a float between `0.0` and `100.0`.
* `query_gpu_power` `-> float`: Returns the power usage of the GPU in milliwatts, if supported by the card.
* `query_gpu_temperature` `-> float`: Returns the current temperature of the GPU in degrees celcius, if supported by the card.

This is only implemented on Windows.

This is only implemented for NVidia video cards.

## Preparing

We need `nvml.dll` on Windows for building against NVidia cards. Install CUDA before building.
Provide the path to the installed CUDA library as `CUDA_PATH`.

Technically, we don't need a working install of CUDA. You can download the full CUDA installer and create a directory
with `nvml.h` in the `./include` directory and `nvml.dll` in the `./lib` path. It will try to find `nvml.dll` via
the `CUDA_PATH`, but you can also specify `SYSTEM_PATH` if we need to look in C:\Windows\System32.

## Building

Update the submodules:

```
git submodule update --init
```

With a working build environment capable of building Godot itself or another extension, run:

```
scons platform=windows target=template_debug
```

To build a release version:

```
scons platform=windows target=template_release
```

You likely need to specify the `CUDA_PATH` and `SYSTEM_PATH`. For WSL1, this would be something like:

```
CUDA_PATH=/mnt/c/Program\ Files/NVIDIA\ GPU\ Computing\ Toolkit/CUDA/v12.2 SYSTEM_PATH=/mnt/c/Windows/System32 scons platform=windows target=template_debug
```

Then copy the files within `dist` to your project. You can elect to only build and then copy the distribution you need for the platform you are developing upon.
