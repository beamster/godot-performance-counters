#!/usr/bin/env python
import re
import os
import sys

# The name of the module
name = "godot-performance-counters"

# Libraries for windows
win_libs = ['pdh', 'psapi','nvml']

# Add specific variables
# Godot version target
godot_version = ARGUMENTS.get("godot_version", "4.1")
if "godot_version" in ARGUMENTS:
    del ARGUMENTS["godot_version"]

# Sanitize godot_version
if not re.match(r"^\d+[.]\d+$", godot_version):
    print("Error: specified `godot_version` must be in the form major.minor such as '4.1'.")
    Exit(1)

godot_target_major, godot_target_minor = godot_version.split('.')

# Ensure godot-cpp repo is cloned at all
os.system(f"git submodule update --init")
os.system(f"cd godot-cpp; git fetch")

# Use that version of godot
godot_cpp_path = os.path.join(os.path.expanduser('~'), '.cache', f"godot-cpp-{godot_version}")
if not os.path.exists(godot_cpp_path):
    os.system(f"git clone godot-cpp {godot_cpp_path} --shared")
if not os.path.exists(f"godot-cpp-{godot_version}"):
    os.symlink(godot_cpp_path, f"godot-cpp-{godot_version}", target_is_directory=True)
os.system(f"cd {godot_cpp_path}; git checkout godot-{godot_version}-stable")

# Import godot-cpp scons
env = SConscript(f"{godot_cpp_path}/SConstruct")

# Flag the godot version
flags = []

# For reference:
# - CCFLAGS are compilation flags shared between C and C++
# - CFLAGS are for C-specific compilation flags
# - CXXFLAGS are for C++-specific compilation flags
# - CPPFLAGS are for pre-processor flags
# - CPPDEFINES are for pre-processor defines
# - LINKFLAGS are for linking flags

# tweak this if you want to use different folders, or more folders, to store your source code in.
flags.append(f"-Isrc")
sources = Glob("src/*.cpp")

env.Append(TARGET_ARCH='i386' if env['platform'].endswith('32') else 'x86_64')

# Negotiate third-party lib path
lib_prefix = f"third-party/{env['platform']}-{env['arch']}"
lib_path = lib_prefix + '/lib'

# Add third-party include path
flags.append(f"-I{lib_prefix}/include")

# The targets we are building and any prerequisites
targets = []
deps = []

# Where we place our own downloaded components
tmp_path = os.path.join('.', 'tmp')

download_cuda = ARGUMENTS.get("download_cuda", False)
if download_cuda:
  os.system('mkdir -p tmp')
  os.system('wget https://developer.download.nvidia.com/compute/cuda/12.5.0/local_installers/cuda_12.5.0_555.85_windows.exe -nc -O tmp/cuda.exe.file')

  # Unpack the specific files we need
  os.system('if [ ! -e tmp/Display.Driver/nvml_loader.dll ]; then 7z x tmp/cuda.exe.file Display.Driver/nvml_loader.dll -otmp; fi')
  os.system('if [ ! -e tmp/Display.Driver/nvml.dll ]; then 7z x tmp/cuda.exe.file Display.Driver/nvml.dll -otmp; fi')
  os.system('if [ ! -e tmp/cuda_nvml_dev/nvml_dev/include/nvml.h ]; then 7z x cuda_12.5.0_555.85_windows.exe cuda_nvml_dev/nvml_dev -otmp; fi')

# Platform specific environment variables
if env['platform'] == 'windows':
    # Get the CUDA install path
    if os.path.exists(os.path.join(tmp_path, 'cuda_nvml_dev', 'nvml_dev', 'include', 'nvml.h')):
        cuda_path = os.path.join(tmp_path, 'cuda_nvml_dev', 'nvml_dev')
    else:
        cuda_path = 'C:\\Program Files\\NVIDIA GPU Computing Toolkit\\CUDA\\v12.2'

    cuda_path = os.environ.get('CUDA_PATH', cuda_path)

    # Detect system32 path for windows
    if os.path.exists(os.path.join(tmp_path, 'Display.Driver', 'nvml.dll')):
        win_sys_path = os.path.join(tmp_path, 'Display.Driver')
    else:
        win_sys_path = 'C:\\Windows\\System32'
    win_sys_path = os.environ.get('SYSTEM_PATH', win_sys_path)

    flags.append(f"-I{os.path.join(cuda_path, 'include')}")

    # Ensure that nvml exists
    if not os.path.exists(os.path.join(win_sys_path, 'nvml.dll')):
        # Check our tmp path
        print("Need nvml.dll to exist within SYSTEM_PATH: " + win_sys_path)
        print("Run with download_cuda=1 to just download a copy of the needed libraries.")
        print("It requires p7zip and wget and about 3GB of network bandwidth and disk space.")
        Exit(1)

    if not os.path.exists(os.path.join(cuda_path, 'include', 'nvml.h')):
        print("Need nvml.h to exist within CUDA_PATH: " + os.path.join(cuda_path, 'include'))
        print("Run with download_cuda=1 to just download a copy of the needed libraries.")
        print("It requires p7zip and wget and about 3GB of network bandwidth and disk space.")
        Exit(1)

    flags.append(f"-D_WIN32_WINNT=0x0600")
    lib_path = lib_prefix + '/bin'

    lib_ext = '.lib'
    if env['use_mingw']:
        lib_ext = '.a'
    for flag in [f"-l{libname}{lib_ext}" for libname in win_libs]:
        flags.append(flag)

    deps.append(env.Command(f'dist/{godot_version}/{name}/{env["platform"]}/{env["arch"]}/nvml.dll',
                               os.path.join(win_sys_path, 'nvml.dll'),
                               Copy('$TARGET', '$SOURCE')))

    deps.append(env.Command(f'dist/{godot_version}/{name}/{env["platform"]}/{env["arch"]}/nvml_loader.dll',
                               os.path.join(win_sys_path, 'nvml_loader.dll'),
                               Copy('$TARGET', '$SOURCE')))

    deps.append(env.Command(target=f'third-party/{env["platform"]}-{env["arch"]}/lib/nvml.def',
                               source=os.path.join(win_sys_path, 'nvml.dll'),
                               action="gendef - $SOURCE > $TARGET"))

    deps.append(env.Command(target=f'third-party/{env["platform"]}-{env["arch"]}/bin/libnvml.a',
                               source=f'third-party/{env["platform"]}-{env["arch"]}/lib/nvml.def',
                               action="x86_64-w64-mingw32-dlltool -k --output-lib $TARGET --input-def $SOURCE"))

# Add library path
flags.append(f"-L{lib_path}")

# Establish output path
output_path = '#bin/' + env['platform'] + '/'
flags.append(f"-L{output_path}")

# Create gdextension file from template
gdextension = env.Command(target=f'dist/{godot_version}/{name}/{name}.gdextension',
                          source='./module.gdextension',
                          action="sed -e 's/gdexample/" + name + f"/g' -e 's/x[.]x/{godot_version}/g' < $SOURCE > $TARGET")

if env["platform"] == "macos":
    library = env.SharedLibrary(
        "dist/{}/{}/osx/bin/lib{}.{}.{}.framework/lib{}.{}.{}".format(
            godot_version, name,
            name, env["platform"], env["target"],
            name, env["platform"], env["target"]
        ),
        source=sources,
        parse_flags=' '.join(flags),
    )
else:
    library = env.SharedLibrary(
        "dist/{}/{}/{}/{}/lib{}{}{}".format(
            godot_version,
            name,
            env["platform"],
            env["arch"],
            name, env["suffix"], env["SHLIBSUFFIX"]
        ),
        source=sources,
        parse_flags=' '.join(flags),
    )

# Tar/Zip distributions
targets.append(
    env.Zip(f"dist/{name}_godot-{godot_version}", [f"dist/{godot_version}"], ZIPROOT=f"dist/{godot_version}")
)

Default(*deps, library, gdextension, *targets)
